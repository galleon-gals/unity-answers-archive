import requests, json, os, time
from bs4 import BeautifulSoup

# Number of pages to scrape questions for
pages = 999

# If to archive directory doesn't exist, create it
if os.path.exists("archive/to_archive") != True:
    os.makedirs("archive/to_archive")

topics = []
for topicGroup in sorted(os.listdir("archive/tags")):
    with open("archive/tags/" + topicGroup, "r") as f:
        topics = json.loads(f.read())
    for topic in topics:
        print("Scraping topic: " + topic)
        questions = []
        for page in range(1, pages):
            res = requests.get("https://answers.unity.com/" + topic + "?page=" + str(page) + "&sort=viewCount&filter=all")
            soup = BeautifulSoup(res.text, "html.parser")
            try:
                for qbox in soup.find("div", {"id":"id_topic_content"}).find_all("div", {"class": "info"}):
                    q = qbox.find("h4", {"class": "title"}).find("a")["href"]
                    if q == "#":
                        raise Exception("No more questions!")
                    questions.append(q)
            except Exception:
                print(Exception)
                break
            print("Scraping page " + str(page))
        with open("archive/to_archive/" + topic.replace("/topics/", "").replace(".html", "") + ".json", "w") as f:
            f.write(json.dumps(questions))
        print("topic: " + topic + ", finished scraping")
        # Uncomment below to slow the scraping, found this to be unnecessary
        #time.sleep(1)
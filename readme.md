To run this project first ensure that you have python 3 installed and a connection to the internet

Next download this repository and from the terminal emulator of your choice navigate to the directory of this project

Run pip3 install -r requirements.txt to install all requirements (beautifulsoup and requests)

Now you will need to run the python scripts in this order:
- topic_scraper.py
- question_url_scraper.py
- questions_scraper.py

Note: this was done quick and dirty since this is under a strict time restraint, please submit an issue if you run into any problems

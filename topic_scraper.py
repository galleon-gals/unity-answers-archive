import requests, json, os
from bs4 import BeautifulSoup

# Number of pages of topics to scrape
pages = 54

# If archive directory doesn't exist create it
if os.path.exists("archive") != True:
    os.makedirs("archive")
# If tags directory doesn't exist create it
if os.path.exists("archive/tags") != True:
    os.makedirs("archive/tags")

for it in range(1, pages + 1):
    r = requests.get("https://answers.unity.com/topics.html?page=" + str(it))
    soup = BeautifulSoup(r.text, "html.parser")
    tags = soup.find("div", {"id": "tagpix"})
    tagList = []
    for tag in tags.find_all("li"):
        for ltag in tag.find_all("a", href=True):
            if "topic" in ltag["href"]:
                tagList.append(ltag["href"])
    with open("archive/tags/tag_page_" + str(it) + ".json", "w") as f:
        f.write(json.dumps(tagList))
    print( str(it) + "/" + str(pages))